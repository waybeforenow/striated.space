SRC     ?= src
STATIC  ?= static
DIST    ?= public

SCHEME ?= gosh
SASSC  ?= sassc
CP     ?= cp

PRODUCE = produce.scm

SXML_SOURCES    = $(foreach s, $(wildcard $(SRC)/*.sxml), $(notdir $(s)))
SCSS_SOURCES    = $(foreach s, $(wildcard $(SRC)/*.scss), $(notdir $(s)))
STATIC_SOURCES  = $(foreach s, $(wildcard $(STATIC)/*),   $(notdir $(s)))
HTML_TARGETS    = $(addprefix $(DIST)/, $(patsubst %.sxml, %.html, $(SXML_SOURCES)))
CSS_TARGETS     = $(addprefix $(DIST)/, $(patsubst %.scss, %.css,  $(SCSS_SOURCES)))
STATIC_TARGETS  = $(addprefix $(DIST)/, $(STATIC_SOURCES))

.PHONY: build

build: $(HTML_TARGETS) $(CSS_TARGETS) $(STATIC_TARGETS)

$(DIST)/%.html: $(SRC)/%.sxml
	mkdir -p $(@D)
	$(SCHEME) $(PRODUCE) $^ $@

$(DIST)/%.css: $(SRC)/%.scss
	mkdir -p $(@D)
	$(SASSC) $^ $@

$(DIST)/%: $(STATIC)/%
	mkdir -p $(@D)
	$(CP) $^ $@

