#!/usr/bin/env gosh

(use sxml.serializer)
(use srfi-19)

(define TODAYS_DATE
  (lambda ()
    (date->string (time-utc->date (current-time)) "~1")))

; deep search through an Sexp looking for any template expression
(define preprocess
  (lambda (xs)
    (if (null? xs) xs
      (if (list? xs)
        (if (eq? (car xs) 'tmpl)
            ; eval the second argument in our environment
            (eval (cadr xs) (interaction-environment))
            ; otherwise recurse through
            (cons (preprocess (car xs)) (preprocess (cdr xs))))
        xs))))

(define main
  (lambda (args)
    (let ((input-file (open-input-file (cadr args)))
          (output-file (open-output-file (caddr args))))
      (display "<!DOCTYPE html>\n" output-file)
      (display (srl:sxml->html (preprocess (port->sexp-list input-file))) output-file)
      0)))

